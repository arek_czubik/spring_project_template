package app.implementation.cities.service;

import app.api.domain.cities.model.City;
import app.api.domain.cities.repository.CityRepository;
import app.api.domain.cities.service.CityService;
import com.google.common.base.Preconditions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional
class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    public CityServiceImpl(CityRepository cityRepository) {
        Preconditions.checkNotNull(cityRepository);

        this.cityRepository = cityRepository;
    }

    @Override
    public Collection<City> getAvailableCities() {
        return cityRepository.getAll();
    }

    @Override
    public void addCity(City city) {
        cityRepository.save(city);
    }
}
