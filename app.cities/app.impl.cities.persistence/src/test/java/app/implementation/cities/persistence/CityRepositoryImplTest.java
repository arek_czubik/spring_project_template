package app.implementation.cities.persistence;

import app.api.domain.cities.model.City;
import app.api.domain.cities.repository.CityRepository;
import app.configuration.CitiesPersistenceConfiguration;
import app.configuration.persistence.PersistenceConfiguration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CitiesPersistenceConfiguration.class, PersistenceConfiguration.class})
@Transactional
@Rollback(true)
class CityRepositoryImplTest {

    @Autowired
    private CityRepository cityRepository;

    @Test
    public void whenGettingAllElementsFromEmptyRepositoryEmptyCollectionIsReturned() {
        // arrange

        // act
        Collection<City> cities = cityRepository.getAll();

        // assert
        assertEquals(0, cities.size());
    }

    @Test
    public void whenAddedCityToEmptyRepositoryItContainsOneElementWithValidProperties() {
        // arrange
        City expected = new City("Kraków", "Polska");

        // act
        cityRepository.save(expected);
        City actual = cityRepository.getAll().stream().findFirst().get();

        // assert
        assertEquals(1,cityRepository.getAll().size());
        assertEquals(expected, actual);
    }

    @Test
    public void whenAddedCityToNonEmptyRepositoryAllExpectedItemsAreAvailable() {
        // arrange
        cityRepository.save(new City("Kraków", "Polska"));
        cityRepository.save(new City("Warszawa", "Polska"));

        // act
        cityRepository.save(new City("Lublin", "Polska"));

        // assert
        assertEquals(3,cityRepository.getAll().size());
    }

    @Test
    public void whenDeletingExistingElementItIsRemovedFromRepository() {
        // arrange
        City existingCity = new City("Kraków", "Polska");
        cityRepository.save(existingCity);

        // act
        City loadedCity = cityRepository.getById(existingCity.getId());
        assertEquals(1, cityRepository.getAll().size());
        cityRepository.delete(loadedCity);

        // assert
        assertEquals(0, cityRepository.getAll().size());
    }

    @Test
    public void whenUpdatedElementNewValueIsAvailableInRepository() {
        // arrange
        City existingCity = new City("Kraków", "Polska");
        cityRepository.save(existingCity);

        // act
        City loadedCity = cityRepository.getById(existingCity.getId());
        loadedCity.setName("Warszawa");
        cityRepository.update(loadedCity);

        // assert
        assertEquals(1, cityRepository.getAll().size());
        City updatedCity = new City("Warszawa", "Polska");
        updatedCity.setId(1L);
        assertEquals(
                updatedCity,
                cityRepository.getAll().stream().findFirst().get());
    }

}