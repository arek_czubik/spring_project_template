package app.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("app.implementation.cities.persistence")
public class CitiesPersistenceConfiguration {
}
