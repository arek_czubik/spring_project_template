package app.implementation.cities.persistence;

import app.api.domain.cities.model.City;
import app.api.domain.cities.repository.CityRepository;
import com.google.common.base.Preconditions;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Repository
@Transactional
class CityRepositoryImpl implements CityRepository {

    private final SessionFactory sf;

    @Autowired
    public CityRepositoryImpl(SessionFactory sessionFactory) {
        Preconditions.checkNotNull(sessionFactory);

        this.sf = sessionFactory;
    }

    @Override
    public City getById(Long id) {
        return sf.getCurrentSession().find(City.class, id);
    }

    @Override
    public Collection<City> getAll() {
        return sf.getCurrentSession().createQuery("select c from City c", City.class).list();
    }

    @Override
    public void save(City city) {
        sf.getCurrentSession().save(city);
    }

    @Override
    public void update(City city) {
        sf.getCurrentSession().update(city);
    }

    @Override
    public void delete(City city) {
        sf.getCurrentSession().delete(city);
    }
}
