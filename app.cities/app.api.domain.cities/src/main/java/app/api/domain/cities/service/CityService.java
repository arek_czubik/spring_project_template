package app.api.domain.cities.service;

import app.api.domain.cities.model.City;

import java.util.Collection;

public interface CityService {

    Collection<City> getAvailableCities();

    void addCity(City city);

}
