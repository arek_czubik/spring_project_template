package app.api.domain.cities.repository;

import app.api.domain.cities.model.City;

import java.util.Collection;

public interface CityRepository {

    City getById(Long id);
    Collection<City> getAll();

    void save(City city);
    void update(City city);
    void delete(City city);

}
