package app.implementation.employees.persistence;

import app.api.domain.employees.model.Department;
import app.api.domain.employees.model.Employee;
import app.api.domain.employees.repository.EmployeesRepository;
import com.google.common.base.Preconditions;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collection;

@Repository
@Transactional
class EmployeesRepositoryImpl implements EmployeesRepository {

    private final SessionFactory sf;

    @Autowired
    public EmployeesRepositoryImpl(SessionFactory sf) {
        Preconditions.checkNotNull(sf);

        this.sf = sf;
    }

    @Override
    public Employee getEmployeeById(Long id) {
        return sf.getCurrentSession().find(Employee.class, id);
    }

    @Override
    public Collection<Employee> getAllEmployees() {
        return sf.getCurrentSession().createQuery("select e from Employee e", Employee.class).list();
    }

    @Override
    public void save(Employee employee) {
        sf.getCurrentSession().save(employee);
    }

    @Override
    public void update(Employee employee) {
        sf.getCurrentSession().update(employee);
    }

    @Override
    public void delete(Employee employee) {
        sf.getCurrentSession().delete(employee);
    }

    @Override
    public Department getDepartmentById(Long id) {
        return sf.getCurrentSession().find(Department.class, id);
    }

    @Override
    public Collection<Department> getAllDepartments() {
        return sf.getCurrentSession().createQuery("select d from Department d", Department.class).list();
    }

    @Override
    public void save(Department department) {
        sf.getCurrentSession().save(department);
    }

    @Override
    public void update(Department department) {
        sf.getCurrentSession().update(department);
    }

    @Override
    public void delete(Department department) {
        sf.getCurrentSession().delete(department);
    }
}
