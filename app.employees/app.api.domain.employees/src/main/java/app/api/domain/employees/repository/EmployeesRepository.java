package app.api.domain.employees.repository;

import app.api.domain.employees.model.Department;
import app.api.domain.employees.model.Employee;

import java.util.Collection;

public interface EmployeesRepository {
    Employee getEmployeeById(Long id);
    Collection<Employee> getAllEmployees();

    void save(Employee employee);
    void update(Employee employee);
    void delete(Employee employee);

    Department getDepartmentById(Long id);
    Collection<Department> getAllDepartments();

    void save(Department department);
    void update(Department department);
    void delete(Department department);
}
