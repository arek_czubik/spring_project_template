package app.api.domain.employees.model;

import com.google.common.base.Preconditions;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@ToString(exclude = "employees")
public class Department {

    @Id
    @Column(name = "company_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_company_id")
    @SequenceGenerator(name = "generator_company_id")
    private Long id;

    @Column(nullable = false)
    @NonNull
    private String name;

    @OneToMany(mappedBy = "hiredInDepartment", fetch = FetchType.EAGER)
    private Collection<Employee> employees = new HashSet<>();

    public void addEmployee(Employee employee) {
        Preconditions.checkNotNull(employee);

        this.employees.add(employee);
    }
}
