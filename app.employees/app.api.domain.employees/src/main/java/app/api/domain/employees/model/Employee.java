package app.api.domain.employees.model;

import com.google.common.base.Preconditions;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Employee {

    @Id
    @Column(name = "employee_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_employee_id")
    @SequenceGenerator(name = "generator_employee_id")
    private Long id;

    @Column(nullable = false)
    @NonNull
    private String employeeId;

    @Column(nullable = false)
    @NonNull
    private String firstName;

    @Column(nullable = false)
    @NonNull
    private String lastName;

    @ManyToOne
    private Department hiredInDepartment;

    public void addToDepartment(Department department) {
        Preconditions.checkNotNull(department);

        this.hiredInDepartment = department;
        department.addEmployee(this);
    }
}
