package app.api.domain.employees.service;

import app.api.domain.employees.model.Department;

import java.util.Collection;

public interface DepartmentService {
    Collection<Department> getAllDepartments();

    Department createNewDepartment(String name);
    void deleteDepartment(Department department);
}
