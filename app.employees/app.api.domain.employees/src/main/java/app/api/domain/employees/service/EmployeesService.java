package app.api.domain.employees.service;

import app.api.domain.employees.model.Employee;
import app.api.domain.employees.model.Department;

import java.util.Collection;

public interface EmployeesService {
    Collection<Employee> getAllEmployees();

    Employee hireEmployee(String firstName, String lastName);
    void fireEmployee(Employee employee);

    void assignEmployeeToDepartment(Employee employee, Department department);
}
