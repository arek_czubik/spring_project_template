package app.implementation.employees.service;

import app.api.domain.employees.model.Department;
import app.api.domain.employees.model.Employee;
import app.api.domain.employees.service.EmployeesService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class EmployeesServiceImpl implements EmployeesService {

    @Override
    public Collection<Employee> getAllEmployees() {
        return null;
    }

    @Override
    public Employee hireEmployee(String firstName, String lastName) {
        return null;
    }

    @Override
    public void fireEmployee(Employee employee) {

    }

    @Override
    public void assignEmployeeToDepartment(Employee employee, Department department) {

    }
}
