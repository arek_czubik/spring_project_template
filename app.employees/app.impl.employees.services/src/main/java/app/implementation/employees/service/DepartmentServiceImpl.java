package app.implementation.employees.service;

import app.api.domain.employees.model.Department;
import app.api.domain.employees.service.DepartmentService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class DepartmentServiceImpl implements DepartmentService{
    @Override
    public Collection<Department> getAllDepartments() {
        return null;
    }

    @Override
    public Department createNewDepartment(String name) {
        return null;
    }

    @Override
    public void deleteDepartment(Department department) {

    }
}
