package app.client;

import app.configuration.ClientCommonConfiguration;
import app.api.domain.cities.model.City;
import app.api.domain.employees.model.Department;
import app.api.domain.employees.model.Employee;
import app.api.domain.employees.repository.EmployeesRepository;
import app.api.domain.cities.service.CityService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    private final static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try (ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(ClientCommonConfiguration.class)) {

            CityService service = ctx.getBean(CityService.class);

            service.addCity(new City("Kraków", "Poland"));
            service.addCity(new City("Wiedeń", "Austria"));

            for(City city : service.getAvailableCities()) {
                System.out.println(city);
            }

            EmployeesRepository employeesRepository = ctx.getBean(EmployeesRepository.class);
            Department crewControl = new Department("Crew Control");
            Department crewManager = new Department("Crew Manager");
            employeesRepository.save(crewControl);
            employeesRepository.save(crewManager);

            for(Department department : employeesRepository.getAllDepartments()) {
                System.out.println(department);
            }

            Employee newEmployee = new Employee("213071", "Arkadiusz", "Czubik");
            newEmployee.addToDepartment(crewControl);
            employeesRepository.save(newEmployee);

            for(Employee employee : employeesRepository.getAllEmployees()) {
                System.out.println(employee);
            }
        }
    }
}
