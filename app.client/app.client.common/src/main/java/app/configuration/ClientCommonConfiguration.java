package app.configuration;

import app.configuration.persistence.PersistenceConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(
        {
                PersistenceConfiguration.class,
                CitiesPersistenceConfiguration.class,
                CitiesServiceConfiguration.class,
                EmployeesPersistenceConfiguration.class,
                EmployeesServiceConfiguration.class
        })
public class ClientCommonConfiguration {
}
