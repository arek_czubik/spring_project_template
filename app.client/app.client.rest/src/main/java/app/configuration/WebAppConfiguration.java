package app.configuration;

import app.api.domain.cities.model.City;
import app.api.domain.cities.service.CityService;
import app.integration.cities.rest.CityRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.annotation.PostConstruct;

@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses = CityRestService.class)
@Import({ClientCommonConfiguration.class})
public class WebAppConfiguration {

    @Autowired
    private CityService cityService;

    @PostConstruct
    private void initializeData() {
        cityService.addCity(new City("Kraków", "Polska"));
        cityService.addCity(new City("Wiedeń", "Austria"));
    }

}
