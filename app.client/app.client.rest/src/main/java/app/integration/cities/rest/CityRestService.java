package app.integration.cities.rest;

import app.api.domain.cities.model.City;
import app.api.domain.cities.service.CityService;
import com.google.common.base.Preconditions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class CityRestService {

    private final CityService cityService;

    public CityRestService(CityService cityService) {
        Preconditions.checkNotNull(cityService);

        this.cityService = cityService;
    }

    @RequestMapping("/city")
    public Collection<City> getAll() {
        return cityService.getAvailableCities();
    }
}
