package app.configuration.persistence;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.PrintWriter;
import java.util.Properties;

@Configuration
@PropertySource("classpath:persistence.properties")
@EnableTransactionManagement
public class PersistenceConfiguration {

    @Value("${datasource.url}")
    private String dataSourceUrl;

    @Value("${datasource.username}")
    private String dataSourceUsername;

    @Value("${datasource.password}")
    private String dataSourcePassword;

    @Value("${hibernate.dialect}")
    private String hibernate_dialect;

    @Value("${hibernate.show_sql}")
    private String hibernate_show_sql;

    @Value("${hibernate.format_sql}")
    private String hibernate_format_sql;

    @Value("${hibernate.hbm2ddl.auto}")
    private String hibernate_hbm2ddl;

    @Value("${hibernate.generate_statistics}")
    private String hibernate_generate_statistics;

    @Bean
    public DataSource dataSource() throws PropertyVetoException {
        Properties props = new Properties();
        props.put("dataSource.logWriter", new PrintWriter(System.out));

        HikariConfig hikariConfig = new HikariConfig(props);
        hikariConfig.setJdbcUrl(dataSourceUrl);
        hikariConfig.setUsername(dataSourceUsername);
        hikariConfig.setPassword(dataSourcePassword);
        hikariConfig.setAutoCommit(false);
        hikariConfig.setDriverClassName("com.mysql.cj.jdbc.Driver");


        HikariDataSource ds = new HikariDataSource(hikariConfig);

        return ds;
//        ComboPooledDataSource ds = new ComboPooledDataSource();
//        ds.setJdbcUrl(dataSourceUrl);
//        ds.setUser(dataSourceUsername);
//        ds.setPassword(dataSourcePassword);
//        ds.setDriverClass("com.mysql.cj.jdbc.Driver");
//        return ds;
    }

    @Bean
    public LocalSessionFactoryBean localSessionFactoryBean(DataSource dataSource) {
        LocalSessionFactoryBean sf = new LocalSessionFactoryBean();
        sf.setPackagesToScan("app.api.domain");
        sf.setDataSource(dataSource);
        sf.setHibernateProperties(createHibernateProperties());
        return sf;
    }

    private Properties createHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", hibernate_dialect);
        props.setProperty("hibernate.dialect.storage_engine", "innodb");
        props.setProperty("hibernate.show_sql", hibernate_show_sql);
        props.setProperty("hibernate.format_sql", hibernate_format_sql);
        props.setProperty("hibernate.hbm2ddl.auto", hibernate_hbm2ddl);
        props.setProperty("hibernate.generate_statistics", hibernate_generate_statistics);
        props.setProperty("hibernate.connection.autocommit", "false");
        return props;
    }

    @Bean
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        return new HibernateTransactionManager(sessionFactory);
    }

}
